import { Match } from "./Match";

export interface StepFunctionContext {
  match: Match;
  status: Match;
  details: Match;
}