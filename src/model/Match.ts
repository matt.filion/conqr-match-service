import { collection } from 's3-db';
import { MatchStat } from './MatchStat';
import { MatchStatus } from './MatchStatus';

@collection({
  name: 'matches',
})
export class Match {
  type: string;
  slug: string;
  id: string;
  gameId: string;
  status: MatchStatus;
  createdOn: Date;
  updatedOn?: Date;
  start?: Date;
  end?: Date;
  closed?: Date;
  stats: MatchStat[];
}
