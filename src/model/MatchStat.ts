import { MatchStatType } from './MatchStatType';

export class MatchStat {
  player: string;
  type: MatchStatType;
  on: Date;
  value: number | string | boolean;
  metadata?: { [key: string]: number | string | boolean };
}
