import { Collection } from 's3-db';
import { Match } from './model/Match';
import { StepFunctionContext } from './model/StepFunctionContext';

const matches: Collection<Match> = new Collection<Match>(Match as any);

export const execute = async (event: StepFunctionContext) => {
  event.match.status = 'DONE';
  event.match.closed = new Date();
  event.match = await matches.save(event.match);
  return event;
};
