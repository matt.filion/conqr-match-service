import { Collection } from 's3-db';
import { Match } from './model/Match';
// import { StepFunctionContext } from './model/StepFunctionContext';

const matches: Collection<Match> = new Collection<Match>(Match as any);

export const execute = async (event: { id: string; type: string; slug: string }) => {
  const { id, type, slug } = event;
  const match: Match = await matches.save({
    id,
    type,
    slug,
    status: 'LOBBY',
    createdOn: new Date(),
    stats: [],
  });
  return { match };
};
